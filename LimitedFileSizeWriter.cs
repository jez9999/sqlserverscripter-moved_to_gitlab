﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SqlServerScripter {
	/// <summary>
	/// Writes UTF8-formatted output to a series of numbered files, whose maximum size will be limited to a given value.
	/// </summary>
	public class LimitedFileSizeWriter : IDisposable {
		#region Private vars
		private BinaryWriter _currentBinaryWriter = null;
		private uint? _maxOutputFileSizeBytes;
		private string _filePath;
		private string _filePrefix;
		private string _fileExtension;
		#endregion

		#region Enums
		/// <summary>
		/// Specifies a newline type.
		/// </summary>
		public enum NewlineType {
			/// <summary>
			/// Normal newline, one byte, 0x0A.
			/// </summary>
			Normal,

			/// <summary>
			/// Windows Notepad-style newline, two bytes, 0x0D 0x0A.
			/// Note that every text editor on Earth other than Windows Notepad can deal with normal newlines.
			/// </summary>
			WindowsNotepad,
		}
		#endregion

		#region Public vars
		/// <summary>
		/// A header to writer at the beginning of each file; its size will count towards the total file size for
		/// the purposes of determining whether the maximum file size has been reached.
		/// </summary>
		public string Header { get; set; }

		/// <summary>
		/// A suffix to write after each piece of text written.
		/// </summary>
		public string Suffix { get; set; }

		/// <summary>
		/// The newline type to be used when terminating a line.  Defaults to a normal newline.
		/// </summary>
		public NewlineType TypeOfNewline { get; set; }

		/// <summary>
		/// Number of bytes written to all files thus far.
		/// </summary>
		public ulong BytesWrittenTotal { get; private set; }

		/// <summary>
		/// Number of bytes written to the current numbered file thus far.
		/// </summary>
		public ulong BytesWrittenCurrentFile { get; private set; }

		/// <summary>
		/// Number of the current numbered file to be written to.
		/// </summary>
		public uint CurrentFileNumber { get; private set; }

		/// <summary>
		/// Path of the current numbered file to be written to.
		/// </summary>
		public string CurrentFilePath { get; private set; }
		#endregion

		#region Private methods
		private string slashNormalize(string strToNormalize) {
			strToNormalize = strToNormalize.Replace('/', '\\');
			return strToNormalize.TrimEnd('\\');
		}

		private void addNewline(List<byte> bytesList) {
			if (TypeOfNewline == NewlineType.WindowsNotepad) {
				bytesList.Add(Convert.ToByte('\r'));
			}
			bytesList.Add(Convert.ToByte('\n'));
		}

		private string addNewline(string input) {
			if (TypeOfNewline == NewlineType.WindowsNotepad) {
				input += '\r';
			}
			input += '\n';

			return input;
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the limited file size writer.
		/// </summary>
		/// <param name="maxOutputFileSizeBytes">Maximum size of each output file.  If null, no file size limit.</param>
		/// <param name="filePath">Path of numbered files to write.</param>
		/// <param name="filePrefix">Prefix of numbered files to write.  If null, no prefix.</param>
		/// <param name="fileExtension">Extension of numbered files to write.  If null, no extension.</param>
		public LimitedFileSizeWriter(uint? maxOutputFileSizeBytes, string filePath, string filePrefix = null, string fileExtension = null) {
			if (maxOutputFileSizeBytes.HasValue && maxOutputFileSizeBytes.Value == 0) {
				throw new ArgumentException("maxOutputFileSizeBytes must not be zero.", "maxOutputFileSizeBytes");
			}
			else if (filePath == null) {
				throw new ArgumentException("filePath must not be null.", "filePath");
			}
			else if (filePrefix != null && (filePrefix.Contains("/") || filePrefix.Contains("\\"))) {
				throw new ArgumentException("If non-null, filePrefix must not contain slashes.", "filePrefix");
			}
			else if (fileExtension != null && (fileExtension.Contains("/") || fileExtension.Contains("\\") || fileExtension.Contains("."))) {
				throw new ArgumentException("If non-null, fileExtension must not contain slashes or dots.", "fileExtension");
			}

			this._maxOutputFileSizeBytes = maxOutputFileSizeBytes;
			this._filePath = filePath;
			this._filePrefix = filePrefix;
			this._fileExtension = fileExtension;
			TypeOfNewline = NewlineType.Normal;
			BytesWrittenTotal = 0;
			BytesWrittenCurrentFile = 0;
			CurrentFileNumber = 0;
		}
		#endregion

		public string CombineStringLines(IEnumerable<string> lines, bool addSuffix = true) {
			StringBuilder sb = new StringBuilder();

			foreach (string line in lines) {
				sb.Append(addNewline(line.Trim(new char[] { '\r', '\n' })));
				if (!string.IsNullOrEmpty(Suffix) && addSuffix) {
					sb.Append(Suffix);
				}
			}

			return sb.ToString();
		}

		public void AppendSuffixLine(string suffixText) {
			AppendSuffix(suffixText, true);
		}

		public void AppendSuffix(string suffixText, bool writeLine = false) {
			Suffix += suffixText;

			if (writeLine) {
				Suffix = addNewline(Suffix);
			}
		}

		public void AppendHeaderLine(string headerText) {
			AppendHeader(headerText, true);
		}

		public void AppendHeader(string headerText, bool writeLine = false) {
			Header += headerText;

			if (writeLine) {
				Header = addNewline(Header);
			}
		}

		public void WriteLine(string output) {
			Write(output, true);
		}

		public void Write(string output, bool writeLine = false) {
			// Get UTF8 bytes for output
			var outputBytes = new List<byte>(Encoding.UTF8.GetBytes(output));

			if (writeLine) {
				addNewline(outputBytes);
			}

			// Modify outputBytes to insert suffix at end
			if (!string.IsNullOrEmpty(Suffix)) {
				outputBytes.AddRange(Encoding.UTF8.GetBytes(Suffix));
			}

			if (_currentBinaryWriter == null || (_maxOutputFileSizeBytes.HasValue && BytesWrittenCurrentFile+(ulong)outputBytes.Count > _maxOutputFileSizeBytes.Value)) {
				// Modify outputBytes to insert header at beginning
				if (!string.IsNullOrEmpty(Header)) {
					outputBytes.InsertRange(0, Encoding.UTF8.GetBytes(Header));
				}

				if (_maxOutputFileSizeBytes.HasValue && outputBytes.Count > _maxOutputFileSizeBytes.Value) {
					throw new Exception("String to output is {0} bytes, but maximum output file size is {1} bytes.  Cannot continue.".FormatWith(outputBytes.Count, _maxOutputFileSizeBytes.Value));
				}

				if (_currentBinaryWriter != null) {
					_currentBinaryWriter.Dispose();
				}

				CurrentFileNumber++;
				BytesWrittenCurrentFile = 0;
				CurrentFilePath = "{0}\\{1}{2}{3}".FormatWith(slashNormalize(_filePath), _filePrefix ?? "", CurrentFileNumber, string.IsNullOrEmpty(_fileExtension) ? "" : "." + _fileExtension);
				try {
					_currentBinaryWriter = new BinaryWriter(
						File.Open(
							CurrentFilePath,
							FileMode.Create,
							FileAccess.Write,
							FileShare.None
						),
						Encoding.UTF8
					);
				}
				catch (Exception ex) {
					throw new Exception("Couldn't open BinaryWriter for file {0}".FormatWith(CurrentFilePath), ex);
				}
			}

			_currentBinaryWriter.Write(outputBytes.ToArray());
			BytesWrittenCurrentFile += (ulong)outputBytes.Count;
			BytesWrittenTotal += (ulong)outputBytes.Count;
		}

		public void Dispose() {
			if (_currentBinaryWriter != null) {
				_currentBinaryWriter.Dispose();
			}
		}
	}
}
