﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlServerScripter {
	public static class StringExtensions {
		/// <summary>
		/// Formats a string using the values of the passed params, in the same way as string.Format does.
		/// </summary>
		/// <param name="format">The string to format.</param>
		/// <param name="values">The params whose values to use in the formatted string.</param>
		/// <returns>The formatted string.</returns>
		public static string FormatWith(this string format, params object[] values) {
			return string.Format(format, values);
		}

		/// <summary>
		/// Removes trailing occurrence(s) of a given string from the current System.String object.
		/// </summary>
		/// <param name="trimSuffix">A string to remove from the end of the current System.String object.</param>
		/// <param name="removeAll">If true, removes all trailing occurrences of the given suffix; otherwise, just removes the outermost one.</param>
		/// <returns>The string that remains after removal of suffix occurrence(s) of the string in the trimSuffix parameter.</returns>
		public static string TrimEnd(this string input, string trimSuffix, bool removeAll = true) {
			while (input != null && trimSuffix != null && input.EndsWith(trimSuffix)) {
				input = input.Substring(0, input.Length - trimSuffix.Length);

				if (!removeAll) {
					return input;
				}
			}

			return input;
		}
	}
}
